//
//  LectureCell.swift
//  CodingChallenge
//
//  Created by Ushan Hattotuwa on 1/12/20.
//  Copyright © 2020 Ushan.h. All rights reserved.
//

import UIKit

class LectureCell: BaseTableCell {

    @IBOutlet weak var lblStartTime:UILabel!
    @IBOutlet weak var lblEndTime:UILabel!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblLecturer:UILabel!
    @IBOutlet weak var lblVenue:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(lecture: LectureModel){

        lblStartTime.text = lecture.startTime
        lblEndTime.text = lecture.endTime
        lblTitle.text = lecture.title
        lblLecturer.text = lecture.lecturer
        lblVenue.text = lecture.venue

    }

}
