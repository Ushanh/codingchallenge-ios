//
//  ShuttleCell.swift
//  CodingChallenge
//
//  Created by Ushan Hattotuwa on 1/12/20.
//  Copyright © 2020 Ushan.h. All rights reserved.
//

import UIKit

class ShuttleCell: BaseTableCell {

    @IBOutlet weak var lblFrom:UILabel!
    @IBOutlet weak var lblTo:UILabel!
    @IBOutlet weak var lblDuration:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(shuttle: ShuttleModel){
        lblFrom.text = shuttle.fromCity
        lblTo.text = shuttle.toCity
        lblDuration.text = shuttle.duration
    }

}
