//
//  TodayCell.swift
//  CodingChallenge
//
//  Created by Ushan Hattotuwa on 1/12/20.
//  Copyright © 2020 Ushan.h. All rights reserved.
//

import UIKit

class TodayCell: UITableViewCell {

    @IBOutlet weak var lblToday:UILabel!
    @IBOutlet weak var vwContainer:UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyles()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func applyStyles(){
        self.separatorInset.right = .greatestFiniteMagnitude
        Helper.roundCorners(view: self.vwContainer, maskedCorners: [.layerMaxXMinYCorner, .layerMinXMinYCorner])
        Helper.addShadow(view: self.vwContainer)
        Helper.roundCorners(view: self.lblToday, maskedCorners: [.layerMinXMinYCorner, .layerMaxXMaxYCorner])
    }

}
