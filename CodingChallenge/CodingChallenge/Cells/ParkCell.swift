//
//  ParkCell.swift
//  CodingChallenge
//
//  Created by Ushan Hattotuwa on 1/12/20.
//  Copyright © 2020 Ushan.h. All rights reserved.
//

import UIKit

class ParkCell: BaseTableCell {

    @IBOutlet weak var lblParkName:UILabel!
    @IBOutlet weak var lblFreeCount:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(park: ParkModel){
        lblParkName.text = park.parkName
        lblFreeCount.text = "\(park.freeSlots)"
    }

}
