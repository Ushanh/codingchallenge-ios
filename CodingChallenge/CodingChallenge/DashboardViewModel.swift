//
//  DashboardViewModel.swift
//  CodingChallenge
//
//  Created by Ushan Hattotuwa on 1/12/20.
//  Copyright © 2020 Ushan.h. All rights reserved.
//

import UIKit

class DashboardViewModel: NSObject {

    var profile: ProfileModel?
    var lectures:[LectureModel]?
    var parkingSlots:[ParkModel]?
    var shuttle:[ShuttleModel]?

    override init() {
        super.init()
        self.loadProfileInfo()
        self.loadLectureInfo()
        self.loadParkingSlots()
        self.loadShuttleInfo()
    }

    func loadProfileInfo(){
        self.profile = ProfileModel(studentName: "Hey, Kier", studyWeek: "17/05 Tuesday, Week 8")
    }

    func loadLectureInfo(){

        let lecture1 = LectureModel(startTime: "8:00 AM", endTime: "10:00 AM", title: "FIT1031 Lecture 01", lecturer: "Arun Kongaurthu", venue: "S4, 13 College Walk, Clayton")
        let lecture2 = LectureModel(startTime: "1:00 PM", endTime: "3:00 PM", title: "FIT1075 Tutorial 11", lecturer: "Jarrod Knibbe", venue: "S3, 13 College Walk, Clayton")
        let lecture3 = LectureModel(startTime: "3:00 PM", endTime: "5:00 PM", title: "FIT1078 Labratory 08", lecturer: "Akshay Sapra", venue: "144, 14 College Walk, Clayton")

        self.lectures = [lecture1, lecture2, lecture3]

    }

    func loadParkingSlots(){
        let slot1 = ParkModel(parkName: "Clayton live feed", freeSlots: 645)
        self.parkingSlots = [slot1]
    }

    func loadShuttleInfo(){
        let shuttle1 = ShuttleModel(fromCity: "Clayton", toCity: "Caulfield", duration: "4 mins")
        let shuttle2 = ShuttleModel(fromCity: "Clayton", toCity: "Peninsula", duration: "16 mins")
        self.shuttle = [shuttle1, shuttle2]
    }

}
