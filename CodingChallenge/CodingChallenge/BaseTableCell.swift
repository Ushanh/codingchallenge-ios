//
//  BaseTableCell.swift
//  CodingChallenge
//
//  Created by Ushan Hattotuwa on 1/12/20.
//  Copyright © 2020 Ushan.h. All rights reserved.
//

import UIKit

class BaseTableCell: UITableViewCell {

    @IBOutlet weak var vwContainer:UIView!
    @IBOutlet weak var lcBottom:NSLayoutConstraint!
    @IBOutlet weak var lcTop:NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setStyle(isFirst:Bool, isLast:Bool){

        //corners
        if isFirst && isLast {
            self.lcTop.constant = 8
            self.lcBottom.constant = 8
            Helper.roundCorners(view: self.vwContainer, maskedCorners: [.layerMaxXMinYCorner, .layerMinXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner])
        }else if isFirst {
            self.lcTop.constant = 8
            self.lcBottom.constant = 0
            Helper.roundCorners(view: self.vwContainer, maskedCorners: [.layerMaxXMinYCorner, .layerMinXMinYCorner])
        }else if isLast {
            self.lcTop.constant = 0
            self.lcBottom.constant = 8
            Helper.roundCorners(view: self.vwContainer, maskedCorners: [.layerMaxXMaxYCorner, .layerMinXMaxYCorner])
        }else{
            self.lcTop.constant = 0
            self.lcBottom.constant = 0
            self.vwContainer.layer.cornerRadius = 0
        }

        //separator
        if isLast {
            self.separatorInset.right = .greatestFiniteMagnitude
        }else{
            self.separatorInset.right = 22
        }

        Helper.addShadow(view: self.vwContainer)

    }

}
