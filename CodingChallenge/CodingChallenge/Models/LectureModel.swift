//
//  LectureModel.swift
//  CodingChallenge
//
//  Created by Ushan Hattotuwa on 1/12/20.
//  Copyright © 2020 Ushan.h. All rights reserved.
//

import Foundation

struct LectureModel {
    var startTime:String
    var endTime:String
    var title:String
    var lecturer:String
    var venue:String
}
