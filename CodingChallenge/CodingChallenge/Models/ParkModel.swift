//
//  ParkModel.swift
//  CodingChallenge
//
//  Created by Ushan Hattotuwa on 1/12/20.
//  Copyright © 2020 Ushan.h. All rights reserved.
//

import Foundation

struct ParkModel {
    var parkName: String
    var freeSlots:Int
}
