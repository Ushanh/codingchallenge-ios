//
//  Helper.swift
//  CodingChallenge
//
//  Created by Ushan Hattotuwa on 1/12/20.
//  Copyright © 2020 Ushan.h. All rights reserved.
//

import UIKit

class Helper: NSObject {

    class func roundCorners(view: UIView, maskedCorners:CACornerMask){
        view.clipsToBounds = true
        view.layer.cornerRadius = 4
        view.layer.maskedCorners = maskedCorners
    }

    class func addShadow(view: UIView){
        view.layer.masksToBounds = false
        view.layer.shadowOpacity = 0.3
        view.layer.shadowRadius = 4.0
        view.layer.shadowOffset = CGSize(width: 0.5, height: 0)
        view.layer.shadowColor = UIColor.gray.cgColor
    }

}
