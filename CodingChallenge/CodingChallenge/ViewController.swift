//
//  ViewController.swift
//  CodingChallenge
//
//  Created by Ushan Hattotuwa on 1/12/20.
//  Copyright © 2020 Ushan.h. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lblStudentName:UILabel!
    @IBOutlet weak var lblStudyWeek:UILabel!
    @IBOutlet weak var tblDashboard:UITableView!

    let viewModel = DashboardViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tblDashboard.tableFooterView = UIView()
        self.loadData()

    }

    func loadData(){

        self.lblStudentName.text = self.viewModel.profile!.studentName
        self.lblStudyWeek.text = self.viewModel.profile!.studyWeek
        self.tblDashboard.reloadData()

    }

}

//MARK: UITableViewDataSource

extension ViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if section == 0 {
            return self.viewModel.lectures!.count + 1
        }else if section == 1 {
            return self.viewModel.parkingSlots!.count
        }else {
            return self.viewModel.shuttle!.count
        }

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let count = tableView.numberOfRows(inSection: indexPath.section)
        let isFirst = indexPath.row == 0
        let isLast = indexPath.row == (count - 1)

        if indexPath.section == 0 {

            if indexPath.row == 0 {

                let cell:TodayCell  = tableView.dequeueReusableCell(withIdentifier: "TodayCell", for: indexPath) as! TodayCell
                return cell

            }else{

                let cell:LectureCell  = tableView.dequeueReusableCell(withIdentifier: "LectureCell", for: indexPath) as! LectureCell
                let lecture = self.viewModel.lectures![(indexPath.row - 1)]
                cell.configure(lecture: lecture)
                cell.setStyle(isFirst: isFirst, isLast: isLast)

                return cell

            }

        }else if indexPath.section == 1 {

            let cell:ParkCell  = tableView.dequeueReusableCell(withIdentifier: "ParkCell", for: indexPath) as! ParkCell

            cell.configure(park: self.viewModel.parkingSlots![indexPath.row])
            cell.setStyle(isFirst: isFirst, isLast: isLast)

            return cell

        }else{

            let cell:ShuttleCell  = tableView.dequeueReusableCell(withIdentifier: "ShuttleCell", for: indexPath) as! ShuttleCell

            cell.configure(shuttle: self.viewModel.shuttle![indexPath.row])
            cell.setStyle(isFirst: isFirst, isLast: isLast)

            return cell

        }

    }

}

//MARK: UITableViewDelegate
extension ViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

        if section == 0 {
            return 0
        }else{
            return 44
        }

    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 44))
        view.backgroundColor = UIColor(displayP3Red: 248.0/255.0, green: 248.0/255.0, blue: 249.0/255.0, alpha: 1.0)
        let label = UILabel(frame: CGRect(x: 20, y: 26, width: view.frame.size.width - 40, height: 18))
        label.font = UIFont.systemFont(ofSize: 14.0, weight: .semibold)
        label.textColor = .gray

        if section == 1 {
            label.text = "Available car parks"
        }else if section == 2 {
            label.text = "Intercampus Shuttle Bus"
        }

        view.addSubview(label)
        return view

    }

}
